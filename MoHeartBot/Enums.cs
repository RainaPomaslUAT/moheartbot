﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoHeartBot
{
    class Enums
    {
        // Deliberately Kept Empty
    }

    public enum Skin
    {
        Fae, Ghost, Ghoul, Hollow, Infernal, Mortal, Queen, Vampire, Werewolf, Witch
    }

    public enum Stats
    {
        Hot = 0, Cold = 1, Volatile = 2, Dark = 3
    }
}
