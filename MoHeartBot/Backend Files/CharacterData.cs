﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoHeartBot.Backend_Files
{
    public class CharacterData
    {
        public string _characterName { get; set; }
        public bool _isNPC { get; set; }

        public Dictionary<string, int> _strings { get; set; }
        public Dictionary<string, int> _stringsOn { get; set; }
        public Dictionary<string, int> _sympTokens { get; set; }
        public List<string> _conditions { get; set; }
        public int _harm { get; set; }
        public int _experience { get; set; }
        public int _advances { get; set; }

        public CharacterData(string name, bool npc = false) {
            _characterName = name;
            _isNPC = npc;
            _strings = new Dictionary<string, int>();
            _stringsOn = new Dictionary<string, int>();
            _sympTokens = new Dictionary<string, int>();
            _conditions = new List<string>();
            _harm = 0;
            _experience = 0;
            _advances = 0;
        }

        public CharacterData() {
        }

        #region "Strings"
        public int AddString(string target, int num = 1) {
            if(num <= 0) {
                return -1;
            }
            if (_strings.ContainsKey(target)) {
                _strings[target] += num;
            } else {
                _strings.Add(target, num);
            }
            return 1;
        }

        public int PullString(string target) {
            if (!_strings.ContainsKey(target)) {
                return -1;
            }
            _strings[target] -= 1;
            if(_strings[target] == 0) {
                _strings.Remove(target);
            }
            return 1;
        }

        public int LoseAllStrings(string target = null) {
            if(target == null) {
                _strings.Clear();
                _sympTokens.Clear();
                return 1;
            } else {
                _strings.Remove(target);
                _sympTokens.Remove(target);
                return 1;
            }
        }

        public int CountStrings(string target) {
            if (!_strings.ContainsKey(target)) {
                return -1;
            }
            return _strings[target];
        }

        public string ShowStrings() {
            string stringsString = _characterName + " currently has the following strings:\n";
            foreach(KeyValuePair<string,int> temp in _strings) {
                if(temp.Value == 1) {
                    stringsString += "They have a string on " + temp.Key + ".\n";
                } else {
                    stringsString += "They have " + temp.Value + " strings on " + temp.Key + ".\n";
                }
            }
            return stringsString;
        }
        #endregion
        #region "Strings On"
        public int AddStringOn(string target, int num = 1) {
            if(num <= 0) {
                return -1;
            }
            if (_stringsOn.ContainsKey(target)) {
                _stringsOn[target] += num;
            } else {
                _stringsOn.Add(target, num);
            }
            return 1;
        }

        public int PullStringOn(string target) {
            if (!_stringsOn.ContainsKey(target)) {
                return -1;
            }
            _stringsOn[target] -= 1;
            if (_stringsOn[target] == 0) {
                _stringsOn.Remove(target);
            }
            return 1;
        }

        public int LoseAllStringsOn(string target) {
            if (!_stringsOn.ContainsKey(target)){
                _stringsOn.Remove(target);
                return 1;
            }
            return -1;
        }

        public int CountStringsOn(string target) {
            if (!_stringsOn.ContainsKey(target)) {
                return -1;
            }
            return _stringsOn[target];
        }

        public string ShowStringsOn() {
            string stringsString = _characterName + " currently has the following strings on them:\n";
            foreach (KeyValuePair<string, int> temp in _stringsOn) {
                if (temp.Value == 1) {
                    stringsString += $"{temp.Key} has a string on them.\n";
                } else {
                    stringsString += $"{temp.Key} has {temp.Value} strings on them.\n";
                }
            }
            return stringsString;
        }
        #endregion
        #region "Sympathetic Tokens"
        public int AddToken(string name, int num = 1) {
            if(num <= 0) {
                return -1;
            }
            if (_sympTokens.ContainsKey(name)) {
                _sympTokens[name] += num;                
            } else {
                _sympTokens.Add(name, num);
            }
            AddString(name, num);
            return 1;
        }

        public int UseToken(string name) {
            if (!_sympTokens.ContainsKey(name)) {
                return -1;
            } else if (PullString(name) == -1) {
                return -1;
            } else {
                _sympTokens[name] -= 1;
                if(_sympTokens[name] == 0) {
                    _sympTokens.Remove(name);
                }
                return 1;
            }
        }

        public int CountTokens(string name) {
            if (!_sympTokens.ContainsKey(name)) return -1;
            return _sympTokens[name];
        }

        public string ListTokens() {
            string tokensString = $"{_characterName} has the following sympathetic tokens: \n";
            foreach(KeyValuePair<string, int> token in _sympTokens) {
                if(token.Value == 1) {
                    tokensString += $"They have a token on {token.Key}\n";
                } else {
                    tokensString += $"They have {token.Value} tokens on {token.Key}\n";
                }
            }
            return tokensString;
        }
        #endregion
        #region "Conditions"
        public string AddCondition(string condition) {
            if (!_conditions.Contains(condition)) {
                _conditions.Add(condition);
                return _characterName + " gained " + condition + " condition.";
            } else {
                return _characterName + " already has the " + condition + " condition.";
            }
        }

        public string RemoveCondition(string condition) {
            if (!_conditions.Contains(condition)) {
                return _characterName + " does not have the " + condition + " condition.";
            }
            _conditions.Remove(condition);
            return _characterName + " has lost the " + condition + " condition.";
        }

        public string ListConditions() {
            string conditionList = _characterName + " has the following conditions: \n";
            foreach(string con in _conditions) {
                conditionList += con + "\n";
            }
            return conditionList;
        }
        #endregion
        #region "Harm"
        public string Harm(int amount) {
            _harm += amount;
            return _characterName + " took " + amount + " harm.\n" + HarmStatus();
        }

        public string Heal (int amount) {
            _harm -= amount;
            if (_harm < 0) _harm = 0;
            return _characterName + " has recovered " + amount + " harm.\n" + HarmStatus();
        }

        public void ClearHarm() {
            _harm = 0;
        }

        public string HarmStatus() {
            return _characterName + " currently has " + _harm + " harm.";
        }
        #endregion
        #region "Experience"
        public string MarkExperience() {
            _experience++;
            if(_experience == 5) {
                _advances++;
                _experience = 0;
                return $"{_characterName} gained an advancement!";
            }
            return $"{_characterName} gained experience.";
        }

        public string ReportExperience() {
            return $"{_characterName} has {_experience} experience and {_advances} advancements.";
        }
        #endregion
    }
}
