﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoHeartBot.Backend_Files
{
    public class CharacterBase
    {
        protected string _userName;
        public string userName {
            get { return _userName; }
        }
        protected string _characterName;
        public string characterName {
            get { return _characterName; }
        }

        protected string[] _look = new string[5];
        protected string[] _eyes = new string[5];
        protected string[] _origin = new string[5];

        protected int _lookIndex;
        protected int _eyesIndex;
        protected int _originIndex;

        protected int[] _stats = new int[4];

        protected List<CharacterString> strings = new List<CharacterString>();
        protected List<string> conditions = new List<string>();

    }

    
}
