﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoHeartBot.Backend_Files
{
    public class CharacterString
    {
        public CharacterBase holder;
        public CharacterBase pcTarget;
        public string target;

        public CharacterString(CharacterBase h, string t = null, CharacterBase pct = null) {
            holder = h;
            if (pct != null) {
                pcTarget = pct;
                target = pcTarget.characterName;
            } else if (t != null) {
                target = t;
            } else {
                throw new System.ArgumentException("Must have either a character target, or a string.");
            }
        }

        public string ToString() {
            return holder.characterName + "has a string on " + target;
        }
    }
}
