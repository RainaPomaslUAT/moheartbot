﻿using Discord.Commands;
using MoHeartBot.Backend_Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace MoHeartBot.Modules
{
    
    public class CharacterModule : ModuleBase<SocketCommandContext> {
        public static Dictionary<string, CharacterData> _characters = new Dictionary<string, CharacterData>();

        [Command("help")]
        [Summary("Lists commands.")]
        public async Task HelpAsync() {
            await Context.Channel.SendMessageAsync($"```\"Character Commands\"\n" +
                                                   $"!add_char Example - Adds a new character with the name Example.\n" +
                                                   $"!remove_char Example - Removes the character example from tracking.\n" +
                                                   $"!list_char - Lists all the characters being tracked.\n" +
                                                   $"!char Example - Lists all the tracked information about Example.\n" +
                                                   $"!harm Example - Causes 1 harm to Example.\n" +
                                                   $"!harm Example x - Causes x harm to Example, where x is an integer.\n" +
                                                   $"!heal Example - Heals 1 harm from Example.\n" +
                                                   $"!heal Example x - Heals x harm from Example, where x is an integer.\n" +
                                                   $"!clear_harm Example - Clears all harm from Example.\n" +
                                                   $"!harm_status Example - Shows Example's harm status.```");
            await Context.Channel.SendMessageAsync($"```\"String Commands\"\n" +
                                                   $"!string add Example Target - Gives Example a string on Target.\n" +
                                                   $"!string add Example Target x - Gives Example x strings on Target.\n"  +
                                                   $"!string pull Example Target - Uses one of Example's strings on Target.\n" +
                                                   $"!string show Example - Shows the strings Example has.\n" +
                                                   $"!string show_on Example - Shows the strings Example has on them.\n" +
                                                   $"!string count Example Target - Shows the number of strings Example has on Target.\n" +
                                                   $"!string count Example Target 1 - Shows the total number of strings Example and Target have on each other.\n" +
                                                   $"!string remove_all Example - Removes all of Example's strings.\n" +
                                                   $"!string remove_all Example Target - Removes all strings Example has on Target.```");
            await Context.Channel.SendMessageAsync($"```\"Condition Commands\"\n" +
                                                   $"!cond add Example morbid - Gives Example the \"morbid\" condition.\n" +
                                                   $"!cond remove Example morbid - Removes the \"morbid\" condition from Example.\n" +
                                                   $"!cond show Example - Lists all of Example's conditions.```");
            await Context.Channel.SendMessageAsync($"```\"Experience Commands\"\n" +
                                                   $"!exp add Example - Gives Example an experience.\n" +
                                                   $"!exp show Example - Lists Example's experience count and advancement count.```");
            await Context.Channel.SendMessageAsync($"```\"Sympathetic Token Commands\"\n" +
                                                   $"!token add Example Target - Gives Example a sympathetic token for Target.\n" +
                                                   $"!token add Example Target x - Gives Example x sympathetic tokens for Target.\n" +
                                                   $"!token use Example Target - Uses one of Example's sympathetic tokens on Target.\n" +
                                                   $"!token show Example - Lists all sympathetic tokens that Example has.\n" +
                                                   $"!token count Example Target - Shows the number of sympathetic tokens Example has on Target.```");

        }

        // !add_char Steve -> Added new character Steve
        // !add_char Steve -> Added new character Steve1
        [Command("add_char")]
        [Summary("Add a new character to track.")]
        public async Task AddCharAsync([Summary("The character's name.")]string name) {
            if (_characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is already being tracked.```");
                return;
            }
            CharacterData toAdd = new CharacterData(name);
            _characters.Add(name, toAdd);
            await Context.Channel.SendMessageAsync($"```Added new character {name}```");
            await SaveData();
        }

        // !list_char -> Steve\n Steve1
        [Command("list_char")]
        [Summary("Lists all characters being tracked.")]
        public async Task ListCharAsync() {
            if(_characters.Count == 0) {
                await Context.Channel.SendMessageAsync("```There are no characters being tracked.```");
                return;
            }
            string response = "```\n";
            foreach(KeyValuePair<string, CharacterData> chara in _characters) {
                response += chara.Key + "\n";
            }
            response += "```";
            await Context.Channel.SendMessageAsync(response);
        }

        [Command("remove_char")]
        [Summary("Removes the specified character.")]
        public async Task RemoveCharAsync([Summary("Character to remove.")]string name) {
            if (!_characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            _characters.Remove(name);
            await Context.Channel.SendMessageAsync($"```{name} has been removed.```");
            await SaveData();
        }

        // !char Steve -> <Steve's Data>
        [Command("char")]
        [Summary("Shows all tracked data for a specific character.")]
        public async Task CharacterDataAsync([Summary("The character's name.")]string name) {
            if (!_characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{_characters[name].ShowStrings()}```");
            await Context.Channel.SendMessageAsync($"```{_characters[name].ShowStringsOn()}```");
            await Context.Channel.SendMessageAsync($"```{_characters[name].ListConditions()}```");
            await Context.Channel.SendMessageAsync($"```{_characters[name].HarmStatus()}```");
            await Context.Channel.SendMessageAsync($"```{_characters[name].ReportExperience()}```");
        }      
        
        [Command("harm")]
        [Summary("Cause harm to the named character.")]
        public async Task HarmAsync([Summary("The character to harm.")]string name, [Summary("The optional amount of harm to cause. Default 1.")]int harm = 1) {
            if (!_characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{_characters[name].Harm(harm)}```");
            await SaveData();
        }

        [Command("heal")]
        [Summary("Heal the named character.")]
        public async Task HealAsync([Summary("The character to heal.")]string name, [Summary("The optional amount to heal. Default 1.")] int heal = 1) {
            if (!_characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{_characters[name].Heal(heal)}```");
            await SaveData();
        }

        [Command("clear_harm")]
        [Summary("Clear all harm off a character.")]
        public async Task ClearHarmAsync([Summary("The character whose harm to clear.")]string name) {
            if (!_characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            _characters[name].ClearHarm();
            await Context.Channel.SendMessageAsync($"```{name} has been cleared of harm.```");
            await SaveData();
        }

        [Command("harm_status")]
        [Summary("Show harm status of character.")]
        public async Task HarmStatusAsync([Summary("The character whose status to check.")]string name) {
            if (!_characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{_characters[name].HarmStatus()}```");            
        }

        public static Task SaveData() {
            JsonSerializerOptions options = new JsonSerializerOptions { WriteIndented = true };
            string jsonString = JsonSerializer.Serialize(_characters, options);
            File.WriteAllText("SaveData.json", jsonString);
            return Task.CompletedTask;
        }

        public static Task LoadData() {
            if (!File.Exists("SaveData.json")) return Task.CompletedTask;
            string jsonString = File.ReadAllText("SaveData.json");
            _characters = new Dictionary<string, CharacterData>();
            _characters = JsonSerializer.Deserialize<Dictionary<string, CharacterData>>(jsonString);
            return Task.CompletedTask;
        }
    }

    [Group("string")]
    public class StringModule : ModuleBase<SocketCommandContext>
    {
        [Command("add")]
        [Summary("Give the specified character a string for the specified target.")]
        public async Task AddStringAsync([Summary("The character to give a string.")]string name, [Summary("The target of the string.")] string target, int num = 1) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            if(num <= 0) {
                await Context.Channel.SendMessageAsync($"```You cannot give zero or fewer strings with this command.```");
                return;
            }
            CharacterModule._characters[name].AddString(target, num);
            if (!CharacterModule._characters.ContainsKey(target)) {
                CharacterModule._characters.Add(target, new CharacterData(target));
            }
            CharacterModule._characters[target].AddStringOn(name, num);
            if (num == 1) {
                await Context.Channel.SendMessageAsync($"```{name} has been given a string on {target}```");
            } else {
                await Context.Channel.SendMessageAsync($"```{name} has been given {num} strings on {target}```");
            }
            await CharacterModule.SaveData();
        }

        [Command("show")]
        [Summary("Show the strings a character has.")]
        public async Task ShowStringsAsync([Summary("The character whose strings will be shown.")]string name) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{CharacterModule._characters[name].ShowStrings()}```");
        }

        [Command("show_on")]
        [Summary("Show the strings a character has on them.")]
        public async Task ShowStringsOnAsync([Summary("The character whose strings on them will be shown.")] string name) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{CharacterModule._characters[name].ShowStringsOn()}```");
        }

        [Command("count")]
        [Summary("Count strings a character has on a target.")]
        public async Task CountStringsAsync([Summary("The character whose strings will be counted.")]string name,
                                            [Summary("The target of the strings.")]string target,
                                            [Summary("Optionally, count combined strings on each other between characters. Put a 1 to do so.")]int combined = 0) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }            
            if (combined != 1) {
                int count = CharacterModule._characters[name].CountStrings(target);
                if(count <= 0) {
                    await Context.Channel.SendMessageAsync($"```{name} has no strings on {target}.```");
                } else {
                    await Context.Channel.SendMessageAsync($"```{name} has {count} strings on {target}.```");
                }
            }
            else {
                if (!CharacterModule._characters.ContainsKey(target)) {
                    await Context.Channel.SendMessageAsync($"```{target} is not found.```");
                    return;
                }
                int count = CharacterModule._characters[name].CountStrings(target) + CharacterModule._characters[name].CountStringsOn(target);
                if(count <= -1) {
                    await Context.Channel.SendMessageAsync($"```{name} and {target} have no strings on each other.```");
                } else if(count == 0) {
                    await Context.Channel.SendMessageAsync($"```Combined, {name} and {target} have one string on each other.```");
                } else {
                    await Context.Channel.SendMessageAsync($"```Combined, {name} and {target} have {count} strings on each other.```");
                }
            }
        }

        [Command("pull")]
        [Summary("The specified character pulls a string on the specified target.")]
        public async Task PullStringAsync([Summary("The character pulling the string.")]string name, [Summary("The target of the string.")]string target) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            if (!CharacterModule._characters.ContainsKey(target)) {
                await Context.Channel.SendMessageAsync($"```{target} is not found.```");
                return;
            }
            int successFlag = 0;
            successFlag += CharacterModule._characters[name].PullString(target);
            successFlag += CharacterModule._characters[target].PullStringOn(name);
            if(successFlag <= 0) {
                await Context.Channel.SendMessageAsync($"```{name} does not have a string on {target}.```");
            } else {
                await Context.Channel.SendMessageAsync($"```{name} pulled a string on {target}.```");
            }
            await CharacterModule.SaveData();
        }

        [Command("remove_all")]
        [Summary("Remove all strings a character has. Specifying a target only removes that target's strings.")]
        public async Task RemoveAllAsync([Summary("The character losing strings.")]string name, [Summary("The optional target.")]string target = null) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            string returnString = "";
            if(target == null) {
                foreach(KeyValuePair<string, CharacterData> character in CharacterModule._characters) {
                    character.Value.LoseAllStringsOn(name);
                }
                returnString = $"```{name} has lost all strings.```";
            } else {
                if (CharacterModule._characters.ContainsKey(target)) {
                    CharacterModule._characters[target].LoseAllStringsOn(name);
                }
                returnString = $"```{name} has lost all strings on {target}```";
            }
            CharacterModule._characters[name].LoseAllStrings(target);
            await Context.Channel.SendMessageAsync(returnString);
            await CharacterModule.SaveData();
        }
    }

    [Group("cond")]
    public class ConditionsModule : ModuleBase<SocketCommandContext>
    {
        [Command("add")]
        [Summary("Add a condition to the character.")]
        public async Task AddConditionAsync([Summary("The character gaining the condition.")]string name, [Summary("The condition.")] string condition) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{CharacterModule._characters[name].AddCondition(condition)}```");
            await CharacterModule.SaveData();
        }

        [Command("show")]
        [Summary("List all conditions on a character.")]
        public async Task ListConditionsAsync([Summary("The character to list conditions of.")]string name) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{CharacterModule._characters[name].ListConditions()}```");
        }

        [Command("remove")]
        [Summary("Remove a condition from the character.")]
        public async Task RemoveConditionAsync([Summary("The character losing the condition.")] string name, [Summary("The condition.")] string condition) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{CharacterModule._characters[name].RemoveCondition(condition)}```");
            await CharacterModule.SaveData();
        }
    }

    [Group("exp")]
    public class ExperienceModule : ModuleBase<SocketCommandContext>
    {
        [Command("add")]
        [Summary("Add experience to the character.")]
        public async Task AddExperienceAsync([Summary("The character to add to.")]string name) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{CharacterModule._characters[name].MarkExperience()}```");
            await CharacterModule.SaveData();
        }

        [Command("show")]
        [Summary("Report character's experience.")]
        public async Task ReportExperienceAsync([Summary("The character to report on.")]string name) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{CharacterModule._characters[name].ReportExperience()}```");
        }
    }

    [Group("token")]
    public class TokenModule : ModuleBase<SocketCommandContext>
    {
        [Command("add")]
        [Summary("Add a token on a target to the character.")]
        public async Task AddTokenAsync([Summary("The character to give a token.")]string name, [Summary("The target of the token.")]string target) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            CharacterModule._characters[name].AddToken(target);
            if (!CharacterModule._characters.ContainsKey(target)) {
                CharacterModule._characters.Add(target, new CharacterData(target));
            }
            CharacterModule._characters[target].AddStringOn(name);
            await Context.Channel.SendMessageAsync($"```{name} has been given a token for {target}.```");
            await CharacterModule.SaveData();
        }

        [Command("use")]
        [Summary("Use a token the character has on a target.")]
        public async Task UseTokenAsync([Summary("The character using a token.")]string name, [Summary("The target of the token.")] string target) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            if (!CharacterModule._characters.ContainsKey(target)) {
                await Context.Channel.SendMessageAsync($"```{target} is not found.```");
                return;
            }
            int successFlag = 0;
            successFlag += CharacterModule._characters[name].UseToken(target);
            successFlag += CharacterModule._characters[target].PullStringOn(name);
            if(successFlag <= 0) {
                await Context.Channel.SendMessageAsync($"```{name} does not have a token on {target}```");
            } else {
                await Context.Channel.SendMessageAsync($"```{name} used a token on {target}```");
            }
            await CharacterModule.SaveData();
        }

        [Command("count")]
        [Summary("Count tokens on a target.")]
        public async Task CountTokensAsync([Summary("The character counting tokens.")]string name, [Summary("The target to be counted.")] string target) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            int count = CharacterModule._characters[name].CountTokens(target);
            if (count <= 0) {
                await Context.Channel.SendMessageAsync($"```{name} does not have any tokens on {target}.```");
            } else {
                await Context.Channel.SendMessageAsync($"```{name} has {count} tokens on {target}.```");
            }
        }

        [Command("show")]
        [Summary("List all tokens the character has.")]
        public async Task ListTokensAsync([Summary("The character listing tokens.")]string name) {
            if (!CharacterModule._characters.ContainsKey(name)) {
                await Context.Channel.SendMessageAsync($"```{name} is not found.```");
                return;
            }
            await Context.Channel.SendMessageAsync($"```{CharacterModule._characters[name].ListTokens()}```");
        }
    }
}
