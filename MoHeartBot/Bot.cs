﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoHeartBot
{
    public class Bot
    {
        private DiscordSocketClient _client;
        private CommandHandler _commandHandler;

        public static void Main(string[] args) => new Bot().MainAsync().GetAwaiter().GetResult();
        
        public async Task MainAsync() {            
            _client = new DiscordSocketClient();
            _client.Log += Log;

            _commandHandler = new CommandHandler(_client, new Discord.Commands.CommandService());       
                  
            string token = "";

            try {
                token = File.ReadAllText("Token.txt");
            } catch (FileNotFoundException) {
                Console.WriteLine("No valid Discord Token found.");
                await Task.Delay(30 * 1000);
                Environment.Exit(0);
            }

            await _client.LoginAsync(Discord.TokenType.Bot, token);
            await _client.StartAsync();

            await _commandHandler.InstallCommandsAync();

            await LoadData();

            while (true) {
                await Task.Delay(5 * 60 * 1000);
                await SaveData();
            }
        }

        private async Task SaveData() {
            await Modules.CharacterModule.SaveData();
        }

        private async Task LoadData() {
           await Modules.CharacterModule.LoadData();
        }

        private Task Log(LogMessage msg) {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
